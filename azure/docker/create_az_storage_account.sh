#/bin/sh

RESOURCE_GROUP=kwik
STORAGE_ACCOUNT_NAME=kwikserverstorage
LOCATION=westeurope
LOG_SHARE_NAME=kwiklogs
CERT_SHARE_NAME=kwikcerts

# Create the storage account with the parameters
az storage account create \
    --resource-group $RESOURCE_GROUP \
    --name $STORAGE_ACCOUNT_NAME \
    --location $LOCATION \
    --sku Standard_LRS

# Create the file shares
az storage share create \
  --name $LOG_SHARE_NAME \
  --account-name $STORAGE_ACCOUNT_NAME

az storage share create \
  --name $CERT_SHARE_NAME \
  --account-name $STORAGE_ACCOUNT_NAME
