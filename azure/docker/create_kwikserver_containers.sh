#!/bin/sh

source ./envvars

if [ -z "$STORAGE_KEY" ]; then
    echo "var not set"
    exit 1
fi

export STORAGE_KEY STORAGE_ACCOUNT_NAME
tmpfile=$(mktemp /tmp/kwikservercontainer.XXXXX)

envsubst < kwikserver_containter_creation.yaml  > $tmpfile

az container create --resource-group kwik --file $tmpfile
rm $tmpfile

