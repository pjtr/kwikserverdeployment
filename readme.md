# kwik server deployment

This repository contains scripts and templates for creating a
[kwik](https://github.com/ptrd/kwik) server deployment in various ways. Kwik is a 100% pure Java implementation of the QUIC protocol.

## Deployment on Azure

### With Docker

See [azure/docker](azure/docker) directory.

The [create_kwikserver_containers.sh](/azure/docker/create_kwikserver_containers.sh) script creates and starts two docker containers: 
one that holds the actual kwik server and one that serves the qlog files. 
The containers use two (Azure Storage) files shares being mapped on docker volumes: "kwikcerts" and "kwiklogs".
The "kwikcerts" share should contain the server certificate and private key, default names for these files
are "fullchain1.pem" and "privkey1.pem", but these can be changed by setting environment variables CERT and KEY.

The docker images are retrieved from docker hub:
- https://hub.docker.com/repository/docker/peterdoornbosch/kwikserver
- https://hub.docker.com/repository/docker/peterdoornbosch/qlogserver



